#include "../include/game.hpp"
#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <fstream>

namespace gnop
{
	Game::Game()
		: gameState(State::mainMenu), gameClock(), running(false), parser("config.ini", config)
	{
		std::ifstream f("config.ini");
		if(f.good())
		{
			f.close();
		}
		else
		{
			f.close();
			parser.writeDefaultConfig();
		}
		parser.readConfig();

		
		std::srand((unsigned int)time(0));
		gameWindow.create(sf::VideoMode(config.getGameWidth(), config.getGameHight()), "GNOP - Ein einfacher PingPong Klone", sf::Style::Close);
		gameWindow.setActive(true);
		gameWindow.setVerticalSyncEnabled(true);
		gameWindow.clear(sf::Color::Black);
		gameWindow.setKeyRepeatEnabled(true);
		sf::Image windowicon;
		if(windowicon.loadFromFile(config.getIconpath()))
		{
			gameWindow.setIcon(16, 16, windowicon.getPixelsPtr());
		}

		paddle1.setFillColor(sf::Color::Blue);
		paddle1.setOrigin((float)config.getPaddleWidth() / 2, (float)config.getPaddleHight() / 2);
		paddle2.setFillColor(sf::Color::Red);
		paddle2.setOrigin((float)config.getPaddleWidth() / 2, (float)config.getPaddleHight() / 2);
		ball.setFillColor(sf::Color::White);

		paddle1.setPosition(sf::Vector2f((float)config.getPaddle1XPos(), (float)config.getPaddle1YPos()));
		paddle2.setPosition(sf::Vector2f((float)config.getPaddle2XPos(), (float)config.getPaddle2YPos()));
		ball.setPosition(sf::Vector2f((float)config.getGameWidth() / 2, (float)config.getGameHight() / 2));

		paddle1.setSize(sf::Vector2f((float)config.getPaddleWidth(), (float)config.getPaddleHight()));
		paddle2.setSize(sf::Vector2f((float)config.getPaddleWidth(), (float)config.getPaddleHight()));
		ball.setRadius(config.getBallRadius());

		mainMenuButtons.push_back(new Button(sf::Vector2i(config.getGameWidth() / 2 - 100, 100), "gfx/ui/button_n.png", "gfx/ui/button_h.png",
											"Start Game!", "resources/font/Carlito-Regular.ttf", 200, 50, std::bind(&Game::startGame, this), *this));
		mainMenuButtons.push_back(new Button(sf::Vector2i(config.getGameWidth() / 2 - 100, 170), "gfx/ui/button_n.png", "gfx/ui/button_h.png",
											"Quit Game!", "resources/font/Carlito-Regular.ttf", 200, 50, std::bind(&Game::quitGame, this), *this));

		pauseMenuButtons.push_back(new Button(sf::Vector2i(config.getGameWidth() / 2 - 100, 100), "gfx/ui/button_n.png", "gfx/ui/button_h.png",
			"Resume Game!", "resources/font/Carlito-Regular.ttf", 200, 50, std::bind(&Game::resumeGame, this), *this));
		pauseMenuButtons.push_back(new Button(sf::Vector2i(config.getGameWidth() / 2 - 100, 170), "gfx/ui/button_n.png", "gfx/ui/button_h.png",
			"Main Menu!", "resources/font/Carlito-Regular.ttf", 200, 50, std::bind(&Game::gotoMainMenu, this), *this));

	}

	Game::~Game()
	{
		for(Button* button : mainMenuButtons)
		{
			delete button;
		}
		mainMenuButtons.clear();

		for(Button* button : pauseMenuButtons)
		{
			delete button;
		}
		pauseMenuButtons.clear();
	}

	void Game::onMainMenu()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				gameWindow.close();
				break;
			case sf::Event::MouseButtonPressed:
			case sf::Event::MouseButtonReleased:
			case sf::Event::MouseMoved:
				updateMainMenuButtons();
				break;
			}
		}
		renderMainMenu();
	}

	void Game::updateMainMenuButtons()
	{
		sf::Time dt = gameClock.restart();
		for(Button* button : mainMenuButtons)
		{
			button->update(dt);
		}
	}

	void Game::renderMainMenu()
	{
		gameWindow.clear(sf::Color::Black);
		for(Button* button : mainMenuButtons)
		{
			button->draw();
		}
		gameWindow.display();
	}

	void Game::onPausing()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				gameWindow.close();
				break;
			case sf::Event::KeyPressed:
				if(event.key.code == sf::Keyboard::Escape)
				{
					gameState = State::playing;
				}
				break;
			case sf::Event::MouseButtonPressed:
			case sf::Event::MouseButtonReleased:
			case sf::Event::MouseMoved:
				updatePauseMenuButtons();
				break;
			}
		}
		renderPauseMenu();
	}

	void Game::updatePauseMenuButtons()
	{
		sf::Time dt = gameClock.restart();
		for(Button* button : pauseMenuButtons)
		{
			button->update(dt);
		}
	}

	void Game::renderPauseMenu()
	{
		for(Button* button : pauseMenuButtons)
		{
			button->draw();
		}
		gameWindow.display();
	}

	void Game::onPlaying()
	{
		sf::Event event;
		while(gameWindow.pollEvent(event))
		{
			switch(event.type)
			{
			case sf::Event::Closed:
				gameWindow.close();
			case sf::Event::KeyPressed:
				switch(event.key.code)
				{
				case sf::Keyboard::Escape:
					gameState = State::pausing;
					break;
				case sf::Event::LostFocus:
					gameState = State::pausing;
					break;
				case sf::Keyboard::W:
				case sf::Keyboard::S:
				case sf::Keyboard::Up:
				case sf::Keyboard::Down:
					pressedKeys.push_back(event.key.code);
					break;
				case sf::Keyboard::Space:
					if(!running)
					{
						running = true;
						paddle1.setPosition(sf::Vector2f((float)config.getPaddle1XPos(), (float)config.getPaddle1YPos()));
						paddle2.setPosition(sf::Vector2f((float)config.getPaddle2XPos(), (float)config.getPaddle2YPos()));
						ball.setPosition(sf::Vector2f((float)config.getGameWidth() / 2, (float)config.getGameHight() / 2));
						do
						{
							angle = (std::rand() % 360) * 2 * 3.1415f / 360;
						} while(std::abs(std::cos(angle)) < 0.7f);
						gameClock.restart();
						pressedKeys.clear();
					}
					break;
				}
			}
		}

		updateGame();
		renderGame();
	}

	void Game::updateGame()
	{
		if(running)
		{
			float deltaTime = gameClock.restart().asSeconds();
			if(!pressedKeys.empty())
			{
				sf::Keyboard::Key key;
				key = pressedKeys.front();
				pressedKeys.pop_front();
				switch(key)
				{
				case sf::Keyboard::W:
					paddle1.setPosition((float)config.getPaddle1XPos(), paddle1.getPosition().y - (float)config.getPaddleSpeed() * deltaTime);
					break;
				case sf::Keyboard::S:
					paddle1.setPosition((float)config.getPaddle1XPos(), paddle1.getPosition().y + (float)config.getPaddleSpeed() * deltaTime);
					break;
				case sf::Keyboard::Up:
					paddle2.setPosition((float)config.getPaddle2XPos(), paddle2.getPosition().y - (float)config.getPaddleSpeed() * deltaTime);
					break;
				case sf::Keyboard::Down:
					paddle2.setPosition((float)config.getPaddle2XPos(), paddle2.getPosition().y + (float)config.getPaddleSpeed() * deltaTime);
					break;
				}
				if(paddle1.getPosition().y - (float)config.getPaddleHight() / 2 < 0.f)
				{
					paddle1.setPosition((float)config.getPaddle1XPos(), (float)config.getPaddleHight() / 2);
				}
				else if(paddle1.getPosition().y + (float)config.getPaddleHight() / 2 > config.getGameHight())
				{
					paddle1.setPosition((float)config.getPaddle1XPos(), (float)config.getGameHight() - config.getPaddleHight() / 2);
				}

				if(paddle2.getPosition().y - ((float)config.getPaddleHight() / 2) < 0.f)
				{
					paddle2.setPosition((float)config.getPaddle2XPos(), (float)config.getPaddleHight() / 2);
				}
				else if(paddle2.getPosition().y + ((float)config.getPaddleHight() / 2) > (float)config.getGameHight())
				{
					paddle2.setPosition((float)config.getPaddle2XPos(), (float)config.getGameHight() - (float)config.getPaddleHight() / 2);
					//paddle2.setPosition(config.getPaddle2XPos(), ((float) config.getGameWidth() - config.getPaddleHight() / 2));
				}

				
			}

			ball.move(std::cos(angle) * (config.getBallSpeed() * deltaTime), std::sin(angle) * (config.getBallSpeed() * deltaTime));
			
			if(ball.getPosition().x - ball.getRadius() < paddle1.getPosition().x + paddle1.getSize().x / 2 &&
				ball.getPosition().x - ball.getRadius() > paddle1.getPosition().x - paddle1.getSize().x / 2 &&
				ball.getPosition().y + ball.getRadius() >= paddle1.getPosition().y - paddle1.getSize().y / 2 &&
				ball.getPosition().y - ball.getRadius() <= paddle1.getPosition().y + paddle1.getSize().y / 2)
			{
				if(ball.getPosition().y > paddle1.getPosition().y)
					angle = 3.1415f - angle + (std::rand() % 20) * 3.1415f / 180;
				else
					angle = 3.1415f - angle - (std::rand() % 20) * 3.1415f / 180;
			}
			if(ball.getPosition().x + ball.getRadius() > paddle2.getPosition().x - paddle2.getSize().x / 2 &&
				ball.getPosition().x + ball.getRadius() < paddle2.getPosition().x/* + paddle2.getSize().x / 2*/ &&
				ball.getPosition().y + ball.getRadius() >= paddle2.getPosition().y - paddle2.getSize().y / 2 &&
				ball.getPosition().y - ball.getRadius() <= paddle2.getPosition().y + paddle2.getSize().y / 2)
			{
				if(ball.getPosition().y > paddle2.getPosition().y)
					angle = 3.1415f - angle + (std::rand() % 20) * 3.1415f / 180;
				else
					angle = 3.1415f - angle - (std::rand() % 20) * 3.1415f / 180;
			}
			if(ball.getPosition().x - ball.getRadius() < 0.f)
			{
				std::cout << "Lost (P1)!" << std::endl;
				running = false;
			}
			if(ball.getPosition().x + ball.getRadius() > config.getGameWidth())
			{
				std::cout << "Lost (P2)!" << std::endl;
				running = false;
			}
			if(ball.getPosition().y + ball.getRadius() > config.getGameHight())
			{
				angle = -angle;
				ball.setPosition(ball.getPosition().x, config.getGameHight() - ball.getRadius() - 0.1f);
			}
			if(ball.getPosition().y - ball.getRadius() < 0)
			{
				angle = -angle;
				ball.setPosition(ball.getPosition().x, ball.getRadius() + 0.1f);
			}
		}
	}

	void Game::renderGame()
	{
		gameWindow.clear(sf::Color::Black);
		gameWindow.draw(paddle1);
		gameWindow.draw(paddle2);
		gameWindow.draw(ball);
		gameWindow.display();
	}

	int Game::loop()
	{
		while(gameWindow.isOpen())
		{
			switch(gameState)
			{
			case State::playing:
				onPlaying();
				break;
			case State::pausing:
				onPausing();
				break;
			case State::mainMenu:
				onMainMenu();
				break;
			default:
				return 2;
			}
		}
		return 0;
	}

	void Game::startGame()
	{
		gameState = State::playing;
		paddle1.setPosition(sf::Vector2f((float)config.getPaddle1XPos(), (float)config.getPaddle1YPos()));
		paddle2.setPosition(sf::Vector2f((float)config.getPaddle2XPos(), (float)config.getPaddle2YPos()));
		ball.setPosition(sf::Vector2f((float)config.getGameWidth() / 2, (float)config.getGameHight() / 2));
		running = false;
	}

	void Game::quitGame()
	{
		gameWindow.close();
	}

	void Game::resumeGame()
	{
		gameState = State::playing;
	}

	void Game::gotoMainMenu()
	{
		gameState = State::mainMenu;
	}
}