#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <fstream>

#include "config.hpp"

namespace gnop
{
	class Parser
	{

	private:
		std::string filepath;
		Config& config;
		
	public:
		Parser(std::string file, Config& cfg);
		void writeDefaultConfig();
		void writeConfig();
		void readConfig();
	private:
		void write(Config& cfg);
		void split(const std::string& src, std::string& var, std::string& val);
	};
}

#endif // PARSER_H

