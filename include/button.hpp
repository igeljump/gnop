#ifndef BUTTON_HPP
#define BUTTON_HPP


#include <SFML/Graphics.hpp>
#include <string>
#include <functional>



namespace gnop
{
	class Game;

	class Button
	{
	private:
		sf::Vector2i pos1;
		sf::Vector2i pos2;
		int width;
		int hight;

		sf::Texture normalTexture;
		sf::Texture activeTexture;
		sf::Texture hoverTexture;
		sf::Text buttonText;
		sf::Font buttonFont;

		sf::Sprite sprite;
		
		Game& game;

		void setActive();
		void setHover();
		void setNormal();

		bool isOnButton(sf::Vector2i);

		std::function<void()> callback;
		sf::Time oldTime;
		const sf::Time clickTime = sf::seconds(0.2f);
	public:
		Button(sf::Vector2i, std::string, std::string, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		Button(sf::Vector2i, std::string, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		Button(sf::Vector2i, std::string, std::string, std::string, int, int, std::function<void()>, Game&);
		~Button();
		
		void draw();
		void update(sf::Time);
	};
}


#endif //BUTTON_HPP